const Sequelize = require('sequelize');
const {snakeCase} = require('lodash');

const configureAssociation = options => Object.assign({
    foreignKey: { allowNull: false }, 
    onUpdate: 'CASCADE', 
    onDelete: 'CASCADE' //'RESTRICT' // Comment this for quicker development. Remember to delete db when changing this! This is because Sequelize does not know how to alter and delete columns with constraints.
}, options);

const defineColumns = (columns, omitDescriptionAndComment = false) => {
    const tableColumns = {};

    if (!omitDescriptionAndComment) {
        Object.assign(tableColumns, {
            description: defineDescriptionColumn(),
        });
    }

    Object.assign(tableColumns, columns);

    if (!omitDescriptionAndComment) {
        Object.assign(tableColumns, {
            comments: Sequelize.TEXT
        });
    }

    return tableColumns;
};

const defineDescriptionColumn = () => ({
    type: Sequelize.TEXT,
    allowNull: false,
    unique: true
});

/*
Columns.addInformativeColumns = columns => ({
    ...Columns.setDescription(),
    ...columns,
    ...Columns.setComments()
});

// Maybe Object.assign cares about column order?

Columns.setDescription = () => ({
    description: {
        type: Sequelize.TEXT,
        allowNull: false,
        unique: true
    }
});

Columns.setComments = () => ({
    comments: Sequelize.TEXT
});

*/

const defineTable = ({name, columns, options}, dbConnection) => 
    dbConnection.define(
        name,
        columns,
        {
            tableName: snakeCase(name),
            ...options
        }
    );

const generateCommonColumns = () => ({
    description: defineDescriptionColumn(),
    comments: Sequelize.TEXT
});

module.exports = {
    configureAssociation,
    defineColumns,
    defineTable,
    defineDescriptionColumn,
    generateCommonColumns,
};
