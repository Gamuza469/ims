const Sequelize = require('sequelize');
const {
    defineDescriptionColumn,
    generateCommonColumns,
    configureAssociation
} = require('../util/model.js.js');

module.exports = {
    defineContainerSchema: dbConnection => {
        const {Item} = dbConnection.models;

        const ContainerTypeMaterial = dbConnection.define(
            'ContainerTypeMaterial',
            generateCommonColumns(),
            {tableName: 'container_type_material'}
        );

        const ContainerTypeLid = dbConnection.define(
            'ContainerTypeLid',
            generateCommonColumns(),
            {tableName: 'container_type_lid'}
        );

        const ContainerType = dbConnection.define(
            'ContainerType', 
            {
                description: defineDescriptionColumn(),
                width: {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    defaultValue: 100
                },
                height: {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    defaultValue: 100
                },
                depth: {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    defaultValue: 100
                },
                comments: Sequelize.TEXT
            },
            {tableName: 'container_type'}
        );

        const Container = dbConnection.define(
            'Container',
            generateCommonColumns(),
            {tableName: 'container'}
        );
        
        Container.items = Container.hasMany(Item, configureAssociation({as: 'items'}));
        Container.containerType = Container.belongsTo(ContainerType, configureAssociation({as: 'containerType'}));
        ContainerType.containerTypeMaterial = ContainerType.belongsTo(ContainerTypeMaterial, configureAssociation({as: 'containerTypeMaterial'}));
        ContainerType.containerTypeLid = ContainerType.belongsTo(ContainerTypeLid, configureAssociation({as: 'containerTypeLid'}));
    }
};
