const Sequelize = require('sequelize');
const {
    defineDescriptionColumn,
    generateCommonColumns,
    configureAssociation
} = require('../util/model.js.js');

module.exports = {
    defineItemSchema: dbConnection => {
        const ItemType = dbConnection.define(
            'ItemType',
            generateCommonColumns(),
            {tableName: 'item_type'}
        );
        const Item = dbConnection.define(
            'Item',
            {
                description: defineDescriptionColumn(),
                comments: Sequelize.TEXT,
                // Force name to snake_case to comply with relational database standards.
                containmentComment: {type: Sequelize.TEXT, field: 'containment_comment'}
            },
            {tableName: 'item'}
        );

        ItemType.item = ItemType.hasMany(Item, configureAssociation({as: 'items'}))
        
        Item.itemType = Item.belongsTo(ItemType, configureAssociation({as: 'itemType'}));
    }
    /*defineItemSchema: dbConnection => {
        const ItemType = dbConnection.define(
            'ItemType',
            generateCommonColumns(),
            {tableName: 'item_type'}
        );
        const Item = dbConnection.define(
            'Item',
            {
                description: defineDescriptionColumn(),
                comments: Sequelize.TEXT,
                // Force name to snake_case to comply with relational database standards.
                containmentComment: {type: Sequelize.TEXT, field: 'containment_comment'}
            },
            {tableName: 'item'}
        );

        ItemType.item = ItemType.hasMany(Item, configureAssociation({as: 'items'}))
        
        Item.itemType = Item.belongsTo(ItemType, configureAssociation({as: 'itemType'}));
    }*/    
};
