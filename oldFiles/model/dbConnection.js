const {defineItemSchema} = require('./item');
const {defineContainerSchema} = require('./container');
//const {defineAcquisitionSchema} = require('./acquisition');
const Sequelize = require('sequelize');

const dbConnection = new Sequelize({
    dialect: 'sqlite',
    storage: './ims.sqlite',
    define: {
        force: true, // Delete all from the beginning.
        freezeTableName: true, // Forcing all tables to have singular (non-pluralized) names.
        paranoid: true, // No hard deletes here.
        underscored: true, // According to relational database standards, names should be snake_case so we force it that way. Only applies to fields with no name specified.
        timestamps: true, // All tables should have timestamps unless specified otherwise.
        version: true // Yeah, to hell with those non-updated stale records...
    },
    logging: false // Shut that gibberish up.
});

defineItemSchema(dbConnection);
defineContainerSchema(dbConnection);
//defineAcquisitionSchema(dbConnection);

module.exports = dbConnection;

// Checking

/* // New Connection class... only useful for using the connection-dependant model definition

class Connection extends Sequelize {
    constructor (options) {
        super(options);
    }

    defineEntity (name, entityDefinition, options = {}) {
        const entityName = capitalize(camelCase(name));
        const tableName = snakeCase(name);

        const model = this.define(
            entityName,
            entityDefinition,
            { tableName, ...options }
        );

        return model;
    }

    defineInformativeEntity (name, entityDefinition, options) {
        return this.defineEntity(
            name,
            Entity.addInformativeFields(entityDefinition),
            options
        );
    }
}

const mainConnection = new Connection({
    dialect: 'sqlite',
    storage: './ims.sqlite',
    define: {
        force: true, // Delete all from the beginning.
        freezeTableName: true, // Forcing all tables to have singular (non-pluralized) names.
        paranoid: true, // No hard deletes here.
        underscored: true, // According to relational database standards, names should be snake_case so we force it that way. Only applies to fields with no name specified.
        timestamps: true // All tables should have timestamps unless specified otherwise.
    },
    logging: false // Shut that gibberish up.
});

module.exports = mainConnection;

*/

/* // Defining entities on a connection

mainConnection.defineEntity(
    'Acquisition',
    { seller: Sequelize.INTEGER }
);

const Acquisition = mainConnection.defineInformativeEntity(
    'Acquisition',
    { seller: Sequelize.INTEGER }
);

module.exports = Acquisition;

*/
