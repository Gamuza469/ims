const dbConnection = require('../model/dbConnection');
const {forEach} = require('lodash');
const {Container, Item, ItemType} = dbConnection.models;

module.exports = async () => {
    const item = await Item.findByPk(1);
    const newItemType = await ItemType.findByPk(2);

    const contaiii = await Container.findByPk(1, {include: [Container.items]});

    forEach(contaiii.items, item => console.log(item.description));

    contaiii.items[0].description = 'clooooooooooooooonic';

    await contaiii.items[0].save();

    // Since items[0] and item are the same, here we will find an optmistic locking error!!

    item.containmentComment = 'Este comentario ha sido editado';
    
    /*await item.setItemType(newItemType); // Auto-saves instantly. Ignores all previously manually edited data!
    await item.save(); // Saves manually edited data. Including the item.containmentComment edit.*/

    await item.reload(); // Cleared item.containmentComment!
    await item.setItemType(newItemType); // Auto-saves instantly. Ignores all previously manually edited data so better do it after saving manually edited fields.
    await item.reload();

    item.containmentComment = 'Este comentario ha sido editado';
    await item.save();

    return;

    // Testing full eager loading.
    const containers = await Container.findAll({include: [{all: true, nested: true}]});
    //console.info(containers[0].items);

    // Mock an Item:item creation from a webform. Supposedly, one would specify the item attributes and then where the item goes (the container).
    const request = {
        itemType: 2,
        container: 1,
        description: 'Un nuevo artículo'
    };

    return Item.create({
        description: request.description,
        container_id: request.container,
        item_type_id: request.itemType
    });
};
