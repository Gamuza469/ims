const dbConnection = require('../model/dbConnection');

module.exports = () => {
    dbConnection.models.Container.drop();
    dbConnection.models.ContainerType.drop();
    dbConnection.models.ContainerTypeMaterial.drop();
    dbConnection.models.ContainerTypeLid.drop();
    
    dbConnection.models.Item.drop();
    dbConnection.models.ItemType.drop();
};
