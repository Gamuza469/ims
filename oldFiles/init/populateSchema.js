const dbConnection = require('../model/dbConnection');
const editSchema = require('./editSchema');
const {Item, ItemType, Container, ContainerType, ContainerTypeLid, ContainerTypeMaterial} = dbConnection.models;

const createNewObject = (object, attributes) => object.create(attributes);

module.exports = async () => {
    const containerTypeLid = await createNewObject(ContainerTypeLid, {description: 'Tapa con pestaña'});
    const containerTypeMaterial = await createNewObject(ContainerTypeMaterial, {description: 'Cartòn'});
    const containerType = await createNewObject(
        ContainerType,
        {
            description: 'Caja de media docena de empanadas',
            container_type_lid_id: containerTypeLid.id,
            container_type_material_id: containerTypeMaterial.id
        }
    );
    const container = await createNewObject(
        Container,
        {
            description: 'Elementos de electricidad',
            container_type_id: containerType.id
        }
    );

    const itemType = await createNewObject(ItemType, {description: 'PlayStation 3 controller'});

    // We can exclude awaiting the creation of a new object here...
    const itemType2 = await createNewObject(ItemType, {description: 'XBox controller'});

    const newItem = await Item.build({
        description: 'Clonic PlayStation 3 controller',
        container_id: container.id
    });

    await newItem.setItemType(itemType, { save: false });

    await newItem.save();

    console.log('Saved here...');

    //console.log(await ItemType.findByPk(1, {include: [{all: true, nested: true}]}));

    /*const item = await createNewObject(
        Item,
        {
            description: 'Clonic PlayStation 3 controller',
            item_type_id: itemType.id,
            container_id: container.id
        }
    );*/

    const item = Item.build({
        description: 'Clonic PlayStation 3.5 controller',
        container_id: container.id
    });

    await item.setItemType(itemType, { save: false });

    await item.save();

    console.log('Saved there...');

    await createNewObject(
        Item,
        {
            description: 'Clonic PlayStation 4 controller',
            item_type_id: itemType.id,
            container_id: container.id
        }
    )

    await createNewObject(
        Item,
        {
            description: 'Clonic PlayStation 1 controller',
            item_type_id: itemType.id,
            container_id: container.id
        }
    )

    await createNewObject(
        Item,
        {
            description: 'Clonic PlayStation 5 controller',
            item_type_id: itemType.id,
            container_id: container.id
        }
    )

    // Test property edition
    item.containmentComment = 'Comentario random desde el código';

    // Did item get eager loaded?

    await item.save();

    return editSchema();
};
