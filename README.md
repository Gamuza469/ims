# Item Management System

It's a Node.js based app which allows the user to get account of all physical items distributed in your house. Starting from the big boxes up to the most diminute piece of paper you need to know where it is at all moments. You forgot where you put your high school photos? Query them. Don't know how many cups you have stashed in your basement? Query them. You now have real control of what you have.

This system uses Sequelize and SQLite plus some npm packages.

Alpha version.

## Summary

In reality this project is an attempt to create a Sequelize framework to simplify and standarize ORM definitions in a specific style.

## TO-DO

A clean-up of the code is in order. Document all the methods and functions is also needed for a consecuent project export. This way we can segregate the framework from the actual **IMS** code... which, in fact, is almost non-exisistant.
