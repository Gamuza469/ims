const {Model} = require('sequelize');
const Entity = require('../extend-sequelize/helpers/Entity');

class ItemType extends Model {
    static name = 'ItemType';
    static entityDefinition = Entity.fetchInformativeDefinition();
    static options = {};
}

module.exports = ItemType;
