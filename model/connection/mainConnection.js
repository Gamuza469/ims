const Acquisition = require('../Acquisition');
const Item = require('../Item');
const ItemType = require('../ItemType');
const sequelizeConnection = require('../../extend-sequelize/connection/SequelizeConnection');
const Entity = require('../../extend-sequelize/helpers/Entity');

const mainConnection = sequelizeConnection({
    dialect: 'sqlite',
    storage: './ims2.sqlite'
});

// First define the entities and their relations...
const entities = [
    Acquisition,
    ItemType,
    Item
];

Item.defineRelations({
    belongsToRelations: {
        itemType: {
            model: ItemType
        }
    }
});

ItemType.defineRelations({
    hasManyRelations: {
        items: {
            model: Item
        }
    }
});

// Then vinculate them to the main connection
Entity.linkEntitiesToConnection(entities, mainConnection);

module.exports = async () => {
    await mainConnection.authenticate();
    console.info('Connected to database successfully.');
    await mainConnection.sync();
    console.info('Database schema has been synced.');
};
