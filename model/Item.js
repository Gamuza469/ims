const {TEXT, Model} = require('sequelize');
const Entity = require('../extend-sequelize/helpers/Entity');

class Item extends Model {
    static name = 'Item';
    static entityDefinition = Entity.obtainDefinitionWithInformativeFields({
        containmentComment: TEXT
    });
    static options = {};

    static singularName = 'item';
    static pluralName = 'items';
}

module.exports = Item;
