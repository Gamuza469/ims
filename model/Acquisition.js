const {Model} = require('sequelize');
const Entity = require('../extend-sequelize/helpers/Entity');

class Acquisition extends Model {
    static name = 'Acquisition';
    static entityDefinition = Entity.fetchInformativeDefinition();
    static options = {};
}

module.exports = Acquisition;
