require('./extend-sequelize/extendSequelize')();
const mainConnection = require('./model/connection/mainConnection');

const express = require('express');
const application = express();
const hostname = 'localhost';
const port = 3000;

const Acquisition = require('./model/Acquisition');
const Item = require('./model/Item');
const ItemType = require('./model/ItemType');

const requestErrorHandler = (error, request, response, next) => {
    console.error(error);
    response.status(500).send('There has been an error with the request!');
    next();
};

const listenerHandler = async error => {
    console.info('Listener attached. Starting server...');

    if (error) {
        return console.error('The server has experienced an error:\n', error);
    }

    await mainConnection();

    console.info('IMS - server has been initiated.');
    console.info(`Server is currently listening at http://${hostname}:${port}/`);
};

const newRequestHandler = async (request, response) => {
    console.info('Process started...');

    const acquisition = Acquisition.build({
        description: 'Bought new PC parts'
    });

    await acquisition.persist();

    const itemType = await ItemType.create({
        description: 'Clonic Playstation 3 controller'
    });

    const item = Item.build({
        description: 'The number one controller'
    });

    await item.setItemType(itemType);
    await item.persist();

    await ItemType.create({
        description: 'Original X-Box controller'
    });
    
    const secondItem = Item.build({
        description: 'My very first original controller'
    });
    await secondItem.setItemType(itemType);
    await secondItem.persist();

    const myItemType = await ItemType.findByPk(1, {include: [{all: true, nested: true}]});
    //console.log('Show items related to this item type', myItemType.lean());

    const myItem = await secondItem.reload({include: [{all: true, nested: true}]});
    //console.log('Show my item and its item type', myItem.lean());

    const myItemItemType = await myItem.getItemType();
    myItemItemType.description = 'Clonic Playstation 3 controller (2019)';

    await myItemItemType.persist();
    const updatedMyItem = await myItem.reload({include: [{all: true, nested: true}]});

    //console.log('dfsfdsfs', myItem.itemType);
    //console.log('asdasda', (await myItem.getItemType()).lean());

    //console.log('Reloaded item...', updatedMyItem.lean());

    //console.log('Check association tree...', await myItem.getItemType());

    console.info('Finished.');
    response.sendFile(__dirname + '/views/index.html');
};

application.get('/', newRequestHandler);
application.use(requestErrorHandler);
application.listen(port, hostname, listenerHandler);
