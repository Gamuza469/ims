const {camelCase} = require('lodash');
const pluralize = require('pluralize');

class Relation {
    static associateOnSave (ParentModel, relationName) {
        const relationSetterName = `set${relationName.charAt(0).toUpperCase()}${camelCase(relationName).slice(1)}`;
        const addChildToRelation = ParentModel.prototype[relationSetterName];
    
        Object.assign(ParentModel.prototype, {
            [relationSetterName]: async function (newChild) {
                // When working with non-nullable foreign keys
                // always set them first using save: false!! on item creation!
                return await addChildToRelation.bind(this)(newChild, { save: false });
            }
        });
    }

    static configure (options) {
        return Object.assign({
            foreignKey: { allowNull: false }, 
            onUpdate: 'CASCADE', 
            onDelete: 'CASCADE' //'RESTRICT' // Set to CASCADE for quicker development. Remember to delete db when changing this! This is because Sequelize does not know how to alter and delete columns with constraints.
        }, options);
    }

    static configureHasManyRelationship (childModelOptions, options) {
        return Object.assign({
            as: {
                singular: `${childModelOptions.singularName || camelCase(childModelOptions.model.name)}`,
                plural: `${childModelOptions.pluralName || pluralize(camelCase(childModelOptions.model.name))}`,
            },
            foreignKey: {
                // Renaming the foreignKey because of the alias specified in the belongsTo definition which changes foreign key to 'alias + Id'
                // However when associating, sequelize will still search for 'unaliasedName + Id' because of the model definition
                // on the new 
                // With this, we instruct sequelize to search inside Item for the 'itemTypeId' foreign key.
                // This way we avoid aliasing issues.
                // However we should fix this somehow...
                name: childModelOptions.foreignKeyName, 
                allowNull: false
            },
            onUpdate: 'CASCADE', 
            onDelete: 'CASCADE' //'RESTRICT'
        }, options);
    }
}

module.exports = Relation;
