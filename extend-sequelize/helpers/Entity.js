const {TEXT} = require('sequelize');

class Entity {
    static obtainDefinitionWithInformativeFields (mainDefinition) {
        const entityDefinition = {};
    
        Object.assign(entityDefinition, this.fetchDescriptionDefinition());
        Object.assign(entityDefinition, mainDefinition);
        Object.assign(entityDefinition, this.fetchCommentsDefinition());
    
        return entityDefinition;
    }

    static fetchDescriptionDefinition () {
        return {
            description: {
                type: TEXT,
                allowNull: false,
                unique: true
            }
        };
    };

    static fetchCommentsDefinition () {
        return {
            comments: TEXT
        };
    }

    static fetchInformativeDefinition () {
        return {
            ...this.fetchDescriptionDefinition(),
            ...this.fetchCommentsDefinition()
        };
    }

    static linkEntitiesToConnection (entities, connection) {
        entities.forEach(entity => entity.initializeOn(connection));
        entities.forEach(entity => entity.applyRelations());
        entities.forEach(entity => entity.associateRelationsOnSave());
    }
}

module.exports = Entity;
