const {Model} = require('sequelize');
const {camelCase, forEach, isEmpty, keys, snakeCase} = require('lodash');
const Relation = require('../extend-sequelize/helpers/Relation');
const pluralize = require('pluralize');

module.exports = () => {
    // Manage associations
    Object.assign(Model, {
        applyRelations: function () {
            if (!this.belongsToRelations && !this.hasManyRelations) {
                return;
            }

            if (!isEmpty(this.belongsToRelations)) {
                const belongsToRelationNames = keys(this.belongsToRelations);
    
                forEach(
                    belongsToRelationNames,
                    relationName => {
                        this.belongsTo(
                            this.belongsToRelations[relationName].model,
                            Relation.configure({as: relationName})
                        )
                    }
                );
            }

            if (!isEmpty(this.hasManyRelations)) {
                const hasManyRelationNames = keys(this.hasManyRelations);

                forEach(
                    hasManyRelationNames,
                    relationName => {
                        const relation = this.hasManyRelations[relationName];

                        this.hasMany(
                            relation.model,
                            Relation.configureHasManyRelationship({
                                foreignKeyName: this.hasManyRelations[relationName].foreignKeyName || `${camelCase(this.name)}Id`,
                                singularName: relation.singularName || pluralize.singular(relationName),
                                pluralName: relation.pluralName || relationName
                            })
                        )
                    }
                );
            }
        },
        associateRelationsOnSave: function () {
            if (isEmpty(this.belongsToRelations)) {
                return;
            }

            forEach(
                keys(this.belongsToRelations),
                relationName => Relation.associateOnSave(this, relationName)
            );
        }
    });

    // Define relations
    Object.assign(Model, {
        defineBelongsToRelations: function (relations) {
            if (!this.belongsToRelations) {
                this.belongsToRelations = {};
            }

            Object.assign(this.belongsToRelations, relations);
        },
        defineHasManyRelations: function (relations) {
            if (!this.hasManyRelations) {
                this.hasManyRelations = {};
            }

            Object.assign(this.hasManyRelations, relations);
        },
        defineRelations: function (relations) {
            if (!this.belongsToRelations) {
                this.belongsToRelations = {};
            }

            if (!this.hasManyRelations) {
                this.hasManyRelations = {};
            }

            Object.assign(this.belongsToRelations, relations.belongsToRelations);
            Object.assign(this.hasManyRelations, relations.hasManyRelations);
        }
    });

    // Schema initialization
    Object.assign(Model, {
        initializeOn: function (connection) { 
            return this.init(
                this.entityDefinition,
                {
                    sequelize: connection,
                    modelName: this.name,
                    tableName: snakeCase(this.name),
                    ...this.options
                }
            );
        },
        findByPkWithRelations: function () {}
    });
    
    // Instance methods
    Object.assign(Model.prototype, {
        lean: function () { return this.get({ plain: true }); },
        persist: async function () {
            await this.save();
            return await this.reload();
        },
        reloadWithRelations: function () {}
    });
};
