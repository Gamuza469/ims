const Sequelize = require('sequelize');

const defaultConfiguration = {
    define: {
        force: true, // Delete all from the beginning.
        freezeTableName: true, // Forcing all tables to have singular (non-pluralized) names.
        paranoid: true, // No hard deletes here. Pretty please.
        underscored: true, // According to relational database standards, names should be snake_case so we force it that way. Only applies to fields with no name specified in the model definition.
        timestamps: true, // All tables should have timestamps unless specified otherwise in the model definition.
        version: true // Yeah, to hell with those non-updated stale records... friggin' force to get latest database values.
    },
    logging: false // Shut that gibberish up.
};

module.exports = userConfiguration => new Sequelize(
    Object.assign(defaultConfiguration, userConfiguration)
);
